namespace tech_test_payment_api.Models
{
    public enum VendasEnum
    {
        AguardandoPagamento,
        PagamentoAprovado,
        Cancelada,
        EnviadoTransportadora,
        Entregue
    }
}